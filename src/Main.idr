module Main

import Ipkg.Commands


data Command
  = New
  | Build
  | Check
  | Test
  | Doc
  | Clean
  | Install
  | Help
  | Unknown

help : String
help = "
pkg -- Command line utility for Idris *.ipkg

new      Create a new package
build    Build package
check    Check package only
test     Run tests for package
doc      Generate IdrisDoc for package
clean    Clean package
install  Install package and the doc
help     Print help for commands
"

total
parseCommand : String -> Command
parseCommand "new"     = New
parseCommand "build"   = Build
parseCommand "check"   = Check
parseCommand "test"    = Test
parseCommand "doc"     = Doc
parseCommand "clean"   = Clean
parseCommand "install" = Install
parseCommand "help"    = Help
parseCommand _         = Unknown

runCommand : Command -> (args: List String) -> IO ()
runCommand New     args = Commands.New.perform args
runCommand Build   args = Commands.Build.perform args
runCommand Check   args = Commands.Check.perform args
runCommand Test    args = Commands.Test.perform args
runCommand Doc     args = Commands.Doc.perform args
runCommand Clean   args = Commands.Clean.perform args
runCommand Install args = Commands.Install.perform args
runCommand Help    []   = putStr help
runCommand Help    (cmd :: args) = runCommand (parseCommand cmd) ("--help" :: args)
runCommand Unknown args =putStr help

main : IO ()
main = do
  (exe :: command :: args) <- getArgs
  runCommand (parseCommand command)  args

