module Ipkg.Commands.Clean

import Ipkg.Files
import System

record Config where
  constructor MkConfig
  isHelp : Bool
  ipkgs : List String

parseArg : List String -> Config
parseArg arg = f (MkConfig False []) arg
where
  f : Config -> List String -> Config
  f config [] = config
  f config ("--help" :: args) = f (record {isHelp = True} config) args
  f config ("--ipkg" :: ipkg :: args) = f (record {ipkgs $= (ipkg::)} config) args

help : String
help = "
ipkg-clean
Clean the project

USAGE:
    ipkg clean [options]

OPTIONS:
    --ipkg <ipkg>  .ipkg File to use (default: all .ipkg in the current directroy). Can be repeated
    --help         Print this help
"


selectIpkgs: Config -> IO (Either FileError (List String))
selectIpkgs config = case ipkgs config of
      [] => findIpkgsIn "."
      xs => pure $ Right xs


export
perform: (args: List String) -> IO ()
perform args = do
  let config = parseArg args
  if isHelp config
  then putStr help
  else do
    Right ps <- selectIpkgs config
    for_ ps $ \pkg =>
      system $ "idris --clean " ++ pkg
