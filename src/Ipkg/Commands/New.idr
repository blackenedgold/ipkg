module Ipkg.Commands.New

import Ipkg.Git

data DirTemplate = File String String | Dir String (List DirTemplate)

printError : FileError -> IO ()
printError err = printLn err


interpretTemplate : DirTemplate -> IO (Either FileError ())
interpretTemplate tmpl = inner "./" tmpl
where
  inner : String -> DirTemplate -> IO (Either FileError ())
  inner dir (File name content) = writeFile (dir ++ "/" ++ name) content
  inner dir (Dir name children) = do
    let dir = dir ++ "/" ++ name
    result <- createDir dir
    foldlM (\prev, tmpl => case prev of
        Right () => inner dir tmpl
        Left err => pure <$> printError err) result children

data ProjectType = Bin | Lib

record Config where
  constructor MkConfig
  isHelp: Bool
  type: ProjectType
  name: Maybe String

binProjectTemplate : String -> Maybe String -> DirTemplate
binProjectTemplate name author =
  let authorLine = case author of
                     Just a => "author = \"" ++ a ++ "\"\n"
                     Nothing => "" in
  Dir name [
    File (name ++ ".ipkg") ("package " ++ name ++ "

version = \"0.1.0\"
" ++ authorLine ++ "
sourcedir = src
modules = Main
main = Main
executable = " ++ name ++ "
pkgs = contrib
"),
    Dir "src" [
      File "Main.idr" "module Main

main : IO ()
main = putStrLn \"Hello, World\"
"
    ]
  ]

libProjectTemplate : String -> Maybe String  -> DirTemplate
libProjectTemplate name author =
  let authorLine = case author of
                     Just a => "author = \"" ++ a ++ "\"\n"
                     Nothing => "" in
  Dir name [
    File (name ++ ".ipkg") ("package " ++ name ++ "

version = \"0.1.0\"
" ++ authorLine ++ "
sourcedir = src
modules = " ++ name ++ ", Tests." ++ name ++ "
tests = Tests." ++ name ++ ".test
pkgs = contrib
"),
    Dir "src" [
      File (name ++ ".idr") ("module " ++ name ++ "

export
one : Integer
one = 1
"),
    Dir "Tests" [
        File (name ++ ".idr") ("module Tests." ++ name ++ "

import Test.Unit.Assertions
import " ++ name ++ "

export
test : IO ()
test = do
  assertEquals one 1
  pure ()
")
      ]
    ]
  ]

genTemplate : Config -> (Maybe GitUser)-> DirTemplate
genTemplate (MkConfig _ Bin (Just name)) user = binProjectTemplate name (map show user)
genTemplate (MkConfig _ Lib (Just name)) user = libProjectTemplate name (map show user)




parseArg : List String -> Config
parseArg arg = f (MkConfig False Bin Nothing) arg
where
  f : Config -> List String -> Config
  f config ("--lib"::args) = f (record {type = Lib} config) args
  f config ("--bin"::args) = f (record {type = Bin} config) args
  f config ("--help"::args) = f (record {isHelp = True} config) args
  f config [name] = record {name = Just name} config
  f config [] = config


help : String
help = "
ipkg-new
Create a new Idris project at <path>

USAGE:
    ipkg new [OPTIONS] <path>

OPTIONS:
    --bin   Use a binary template
    --lib   Use a library template
    --help  Print this help
ARGS:
    <path>
"

export
perform : (args: List String) -> IO ()
perform args = do
  let config = parseArg args
  if isHelp config
  then putStr help
  else do
    user <- gitUser
    result <- interpretTemplate $ genTemplate config user
    case result of
      Right () => pure ()
      Left err => printError err

