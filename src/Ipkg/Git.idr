module Ipkg.Git

cmdOutput :  String -> IO (Either FileError String)
cmdOutput cmd = do
  Right f <- popen cmd Read
   | Left e => pure (Left e)
  Right output <- fGetLine f
   | Left e => pure (Left e)
  pclose f
  pure $ Right output




gitName : IO (Either FileError String)
gitName = map (map trim) $ cmdOutput "git config user.name"

gitEmail : IO (Either FileError String)
gitEmail = map (map trim) $ cmdOutput "git config user.email"

export
record GitUser where
  constructor MkGitUser
  name, email: String

export
gitUser : IO (Maybe GitUser)
gitUser = do
  Right name <- gitName
    | Left => pure Nothing
  Right email <- gitEmail
    | Left => pure Nothing
  pure $ Just $ MkGitUser name email


export
Show GitUser where
  show (MkGitUser name email) = name ++ " <" ++ email ++ ">"
